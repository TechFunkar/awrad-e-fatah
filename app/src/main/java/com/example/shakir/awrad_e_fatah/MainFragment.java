package com.example.shakir.awrad_e_fatah;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */

public class MainFragment extends Fragment implements View.OnClickListener {

    Button btRead,btListen,btAbout;
    TextView tvLabel;
    View view;
    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_main, container, false);
        btRead = (Button) view.findViewById(R.id.bt_read);
        btListen = (Button) view.findViewById(R.id.bt_listen);
        btAbout = (Button) view.findViewById(R.id.bt_about);
        tvLabel = (TextView) view.findViewById(R.id.tv_label);

        btRead.setOnClickListener(this);
        btListen.setOnClickListener(this);
        btAbout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bt_read:
                Toast.makeText(getActivity(),"Read Clicked..!!",Toast.LENGTH_LONG).show();  // Toast within a Fragment
                tvLabel.setText("Clicked Read..!!");
                //Launch Another Activity
                Intent intent = new Intent(getActivity(),ReadActivity.class);
                startActivity(intent);
                break;
            case R.id.bt_listen:
                tvLabel.setText("Clicked Listen..!!");
                break;
            case R.id.bt_about:
                tvLabel.setText("Clicked About..!!");
                intent = new Intent(getActivity(),AboutActivity.class);
                startActivity(intent);
                break;
        }
    }
}
