package com.example.shakir.awrad_e_fatah;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toast;

public class ReadActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        showReadFragment();
    }
    void showReadFragment(){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, new ReadFragment());
        fragmentTransaction.commit();
        Toast.makeText(this, "Read Fragment Launched..!!", Toast.LENGTH_LONG).show();
    }

}
